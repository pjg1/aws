#!/bin/bash

# Build an AMI using Packer

# set -e stops the execution of a script if a command or pipeline has an error - which is the opposite of the default shell behaviour, which is to ignore errors in scripts.
# https://stackoverflow.com/questions/39162846/what-does-set-e-and-set-a-do-in-bash-what-are-other-options-that-i-can-use-wit
set -e

# If 1 is null or unset, ami.json is substituted for 1. The value of 1 does not change.
AMI_SPEC=${1:-"ami.json"}

#AMI="ami-6df1e514"
#NAME="BitTiger-Amazon-Linux-AMI-2017.01.1-x86_64-HVM-SSD"
#ARGS="-var source_ami_id=${AMI} -var ami_name_prefix=${NAME} ${AMI_SPEC}"
#echo "Build customized Amazon Linux AMI from ${AMI} ..."
#packer validate ${ARGS}
#packer build ${ARGS}

# basic AMI provided by AWS
AMI="ami-6df1e514"
NAME="BitTiger-Amazon-ECS-AMI-2016.09.f-x86_64-HVM-GP2"

# -var argument outside is passed into ami.json
ARGS="-var source_ami_id=${AMI} -var ami_name_prefix=${NAME} ${AMI_SPEC}"
echo "Build customized Amazon Linux AMI from ${AMI} ..."

# invoke packer with ARGS
packer validate ${ARGS}
packer build ${ARGS}
