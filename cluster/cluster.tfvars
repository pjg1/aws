# AWS Settings
aws = {
  region         = "us-west-2"
  key_name       = "keyPair"                                # TODO
  ami            = "ami-0e4d8a76"                      # TODO
  subnet_id      = "subnet-9e4af9c5"
  subnet_ids     = "subnet-9e4af9c5,subnet-0f4ffc54"    # TODO
  route53_zone   = "Z3J8ETG8HY05XY"                     # TODO
  monitoring     = "true"
  vpc_id         = "vpc-f110dc97"                       # TODO
  associate_public_ip_address = "true"
  in_ssh_cidr_block    = "0.0.0.0/0"
  iam_instance_profile = "Taxi-EC2-Instance-Profile"    # TODO  extension proj, can dynamically create instance profile
  use_spot_instances   = false
  use_load_balancer    = false # TODO, make it true for ELB
}

# Terraform Settings
terraform = {
  backend = "s3"
  region  = "us-west-2"
  bucket  = "pjg1-us-west-2" # TODO
}

# Tags
tags = {
  environment = "demo"
  user        = "pjg1"       # TODO
}

# Web Server Settings
webserver = {
  instance_type        = "t2.micro"
  # create 2 web server for ELB
  count                = "1"
  root_volume_type     = "gp2"
  root_volume_size     = "8"
  root_volume_delete   = "true"
  in_http_cidr_block   = "0.0.0.0/0"
}

# Mapper Settings
mapper = {
  instance_type        = "c4.2xlarge"
  count                = "2" # not on demand
  spot_price           = "0.7" # duplicate with spot_prices in this file
  ebs_device_name      = "/dev/sdb"
  ebs_volume_size      = 2
  ebs_volume_type      = "gp2"
  ebs_volume_deletion  = "true"
  out_sqs_cidr_block   = "0.0.0.0/0"

  use_as_ecs           = false
  use_asg              = false
  asg_instance_types   = "c4.large,c4.2xlarge" #c4.4xlarge
  asg_instance_counts  = "1,1"
  asg_termination_policies = "ClosestToNextInstanceHour,OldestInstance" # in slides

  use_spotfleet        = false
  spot_instance_types  = "c4.large" # can right same way as above asg
  spot_instance_counts = "2"
  spot_prices          = "0.5"
  spot_iam_role = "arn:aws:iam::188291523853:role/MapperSpotFleetRole"  # TODO
  spot_allocation_strategy = "lowestPrice"
  spot_valid_until = "2018-01-01T00:00:00Z"
  spot_availability_zone = "us-west-2c"
}

# Reducer Settings
# not used for now in this case, output directly to dynamodb
reducer = {
  instance_type        = "c4.8xlarge"
  count                = "0"
  spot_price           = "1.0"
}

# Docker Settings
docker = {
  ami                  = "ami-0e4d8a76"    #"ami-3aa8b443"
  instance_type        = "t2.micro"
  count                = "0"
  spot_price           = "0.025"
}
