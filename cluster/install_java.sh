#!/bin/bash

VERSION=8

#JAVA_RPM_URL=http://download.oracle.com/otn-pub/java/jdk/8u131-b11/jdk-8u131-linux-x64.rpm
JAVA_RPM_URL=http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.rpm
if [ "$VERSION" == "7" ]; then
    JAVA_RPM_URL=http://download.oracle.com/otn-pub/java/jdk/7u79-b15/jdk-7u79-linux-x64.rpm
fi

# Oracle made download unavailable...skip Java for now
# TODO: use S3 to distribute instead
exit 0

set -e
set -x

wget --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" $JAVA_RPM_URL -O jdk-$VERSION-linux-x64.rpm

rpm -Uvh jdk-$VERSION-linux-x64.rpm

alternatives --install /usr/bin/java java /usr/java/latest/bin/java 2

rm -rf jdk-$VERSION-linux-x64.rpm
