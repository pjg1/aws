#!/bin/bash -v
# used in auto scaling group;customization; shell script attach and run after instance run

SETUP_SCRIPT=pjg1-us-west-2/aws/setup.sh
SETUP_USER=ec2-user

aws s3 cp s3://${SETUP_SCRIPT} /home/${SETUP_USER}/setup.sh
chown ${SETUP_USER}:${SETUP_USER} /home/${SETUP_USER}/setup.sh
chmod 700 /home/${SETUP_USER}/setup.sh
# if user data changed, must destroy auto scaling group, so decouple logic into setup.sh
su - ${SETUP_USER} -c "/home/${SETUP_USER}/setup.sh"
